package imc.exercise.group4.project1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Imc4Application {

	public static void main(String[] args) {
		SpringApplication.run(Imc4Application.class, args);
	}
}
